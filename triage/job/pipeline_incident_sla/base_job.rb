# frozen_string_literal: true

require 'date'
require 'slack-messenger'
require_relative '../../triage/job'

module Triage
  module PipelineIncidentSla
    class BaseJob < Job
      SLACK_ICON = ':gitlab-bot:'
      SLACK_USERNAME = 'gitlab-bot'
      DEV_ESCALATION_CHANNEL_ID = 'CLKLMSUR4'
      TRIAGE_DRI_HANDBOOK_URL = 'https://handbook.gitlab.com/handbook/engineering/workflow/#triage-dri-responsibilities'
      DEVELOPMENT_CHANNEL = 'development'
      EXCLUDED_STAGES = ['Engineering Productivity'].freeze

      private

      def execute(event)
        prepare_executing_with(event)

        post_slack_reminder if applicable?
      end

      def post_slack_reminder
        messenger.ping(text: slack_message)
      end

      def applicable?
        incident.state == 'opened' && exceeding_response_time_threshold?
      end

      def exceeding_response_time_threshold?
        (Time.now - Time.parse(incident.updated_at)) >= threshold_in_seconds
      end

      def attributed_group
        group_label = (Hierarchy::Group.all_labels & incident.labels).first
        return unless group_label

        Hierarchy::Group.find_by_label(group_label)
      end

      def group_slack_channel
        attributed_group&.slack_channel
      end

      def attributed_stage
        return attributed_group.stage unless attributed_group.stage.nil? || EXCLUDED_STAGES.include?(attributed_group.stage.name)
      end

      def stage_slack_channel
        return DEVELOPMENT_CHANNEL unless attributed_stage&.slack_channel && attributed_stage.slack_channel != ''

        attributed_stage.slack_channel
      end

      def incident
        @incident ||= Triage.api_client.issue(event.project_id, event.iid)
      end

      def messenger
        Slack::Messenger.new(ENV.fetch('SLACK_WEBHOOK_URL', nil), slack_options)
      end

      def slack_options
        {
          channel: slack_channel,
          username: SLACK_USERNAME,
          icon_emoji: SLACK_ICON
        }
      end

      def threshold_in_seconds
        raise NotImplementedError
      end

      def slack_channel
        raise NotImplementedError
      end

      def slack_message
        raise NotImplementedError
      end
    end
  end
end
