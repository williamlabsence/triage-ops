# frozen_string_literal: true

require_relative './base_job'

module Triage
  module PipelineIncidentSla
    class GroupReminderJob < BaseJob
      DELAY = 10 * 60 # 10 minutes
      NEXT_PING_DELAY_IN_MINUTES = 50

      SLACK_PAYLOAD_TEMPLATE = <<~MESSAGE.chomp
        This channel is the triage DRI for pipeline incident <%<incident_url>s|#%<incident_iid>s>. Please take action following the <%<handbook_url>s|Broken master handbook page>!
        I will ping the channel again in %<next_ping_delay>s minutes of inactivity. Thanks for your attention!
      MESSAGE

      private

      def applicable?
        super && attributed_group
      end

      def threshold_in_seconds
        DELAY
      end

      def slack_channel
        group_slack_channel
      end

      def slack_message
        format(SLACK_PAYLOAD_TEMPLATE,
          handbook_url: TRIAGE_DRI_HANDBOOK_URL,
          incident_iid: event.iid,
          incident_url: event.url,
          next_ping_delay: NEXT_PING_DELAY_IN_MINUTES
        )
      end
    end
  end
end
