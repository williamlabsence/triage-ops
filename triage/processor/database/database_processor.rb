# frozen_string_literal: true

require_relative '../../triage/event'
require_relative '../../triage/processor'
require_relative '../../triage/unique_comment'
require_relative '../../../lib/constants/labels'

module Triage
  class DatabaseProcessor < Processor
    def database_reviewed_or_approved?
      event.label_names.include?(Labels::DATABASE_APPROVED_LABEL) ||
        event.label_names.include?(Labels::DATABASE_REVIEWED_LABEL)
    end

    def not_spam?
      !event.label_names.include?(Labels::SPAM_LABEL)
    end
  end
end
