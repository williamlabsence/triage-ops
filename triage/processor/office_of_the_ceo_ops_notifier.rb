# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'
require_relative '../triage/changed_file_list'

module Triage
  class CeoChiefOfStaffTeamOpsNotifier < Processor
    OFFICE_OF_THE_CEO_OPS_PATHS = [
      'handbook/ceo/office-of-the-ceo/_index.md'
    ].freeze

    OFFICE_OF_THE_CEO_OPS_CODEOWNERS_PATHS = [
      '.gitlab/CODEOWNERS'
    ].freeze

    LABELS_FOR_FILE = {
      '.gitlab/CODEOWNERS' =>
        ['Office of the CEO', 'handbook::content'].freeze,
      'handbook/ceo/office-of-the-ceo/_index.md' =>
        ['Office of the CEO', 'handbook::content'].freeze
    }.freeze

    react_to 'merge_request.update', 'merge_request.open'

    def applicable?
      event.from_www_gitlab_com_or_handbook? &&
        (office_of_the_ceo_ops_related_change? || office_of_the_ceo_ops_codeowners_related_change?) &&
        unique_comment.no_previous_comment?
    end

    def process
      add_comment(review_request_comment, append_source_link: true)
    end

    def documentation
      <<~TEXT
        This processor requests DRIs to review merge requests when changes are done to pages related to the Office of the CEO
      TEXT
    end

    private

    def office_of_the_ceo_ops_related_change?
      changed_file_list.any_change?(OFFICE_OF_THE_CEO_OPS_PATHS)
    end

    def office_of_the_ceo_ops_codeowners_related_change?
      changed_file_list.any_change?(OFFICE_OF_THE_CEO_OPS_CODEOWNERS_PATHS)
    end

    def changed_file_list
      @changed_file_list ||= Triage::ChangedFileList.new(event.project_id, event.iid)
    end

    def review_request_comment
      comment = if office_of_the_ceo_ops_codeowners_related_change?
                  <<~MARKDOWN.chomp
                    When you are ready for this MR to be reviewed please tag `@gitlab-com/office-of-the-ceo` as it modifies a `.gitlab/CODEOWNERS` that they maintain.

                    #{labels}
                  MARKDOWN
                else
                  <<~MARKDOWN.chomp
                    When you are ready for this MR to be reviewed please tag `@gitlab-com/office-of-the-ceo` as it modifies files related to the [Office of the CEO](https://handbook.gitlab.com/handbook/ceo/office-of-the-ceo/).

                    #{labels}
                  MARKDOWN
                end

      unique_comment.wrap(comment).strip
    end

    def labels
      labels = changed_file_list.merge_request_changes.flat_map do |change|
        labels_for_change(change)
      end.uniq

      return '' if labels.empty?

      "/label #{labels.map { |label| "~\"#{label}\"" }.join(' ')}"
    end

    def labels_for_change(change)
      Triage::ChangedFileList::OLD_NEW_PATHS.flat_map do |old_or_new|
        path = change[old_or_new]
        LABELS_FOR_FILE[path] || []
      end
    end
  end
end
