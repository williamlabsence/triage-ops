# frozen_string_literal: true

require_relative 'technical_writing_label_base'

module Triage
  module Docs
    class TechnicalWritingLabelMerged < TechnicalWritingLabelBase
      react_to 'merge_request.merge'

      def documentation
        <<~TEXT
          This processor ensures the 'Technical Writing' label is applied
          to merge requests merged by members of the Docs team.
        TEXT
      end
    end
  end
end
