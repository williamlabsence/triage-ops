# frozen_string_literal: true

require_relative '../../../lib/constants/labels'
require_relative '../../triage/processor'
require_relative '../../job/closed_issue_workflow_label_updater_job'

module Triage
  module Workflow
    class ClosedIssueWorkflowLabelUpdater < Processor
      react_to 'issue.close'

      def applicable?
        event.from_gitlab_org? &&
          event.label_names.include?(Labels::WORKFLOW_VERIFICATION)
      end

      def process
        ClosedIssueWorkflowLabelUpdaterJob.perform_in(
          Triage::DEFAULT_ASYNC_DELAY_MINUTES,
          event
        )
      end

      def documentation
        <<~TEXT
        This processor changes closed issues from workflow::verification to verification::complete
        TEXT
      end
    end
  end
end
