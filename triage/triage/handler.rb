# frozen_string_literal: true

require_relative 'listener'

Dir[File.expand_path("../processor/**/*.rb", __dir__)].each { |f| require f }

module Triage
  class Handler
    DEFAULT_PROCESSORS = [
      ApplyTypeLabelFromRelatedIssue,
      AssignDevForVerification,
      AvailabilityPriority,
      BreakingChangeComment,
      BrokenMasterLabelNudger,
      CeoChiefOfStaffTeamOpsNotifier,
      CiComponentsLabelProcessor,
      CopySecurityIssueLabels,
      CustomerLabel,
      DefaultLabelUponClosing,
      DocsOnlyLabeller,
      EngineeringAllocationLabelsReminder,
      LabelInference,
      LabelJiHuContribution,
      LegalDisclaimerOnDirectionResources,
      MrApprovedLabel,
      NewPipelineOnApproval,
      PajamasMissingWorkflowLabelOrWeight,
      PipelineTierLabel,
      ProdOpsFlowNotifier,
      ReactToChanges,
      RemindMergedMrDeviatingFromGuideline,
      RequireTypeOnRefinement,
      RevertMrTemplateNudger,
      ScheduledIssueTypeLabelNudger,
      SeekingCommunityContributionsLabel,
      StableE2eCommentOnApproval,
      TeamLabelInference,
      TypeLabelNudger,
      UxPaperCutsMrs,

      # Internal gitlab-bot command
      CommandRetryPipelineOrJob,

      # AppSec processors
      ApprovedByAppSec,
      AppSecApprovalLabelAdded,
      PingAppSecOnApproval,
      RevokeAppSecApproval,
      CommentOnPublicMRReferencingVulnerability,

      # Community processors
      AutomatedReviewRequestDoc,
      AutomatedReviewRequestGeneric,
      AutomatedReviewRequestUx,
      CodeReviewExperienceFeedback,
      CommandMrFeedback,
      CommandMrHandoff,
      CommandMrHelp,
      CommandMrLabel,
      CommandMrRequestReview,
      CommandMrUnassignReview,
      DetectAndFlagSpam,
      FirstContributionMergedMessage,
      GrowthAffectingNotifier,
      HackathonLabel,
      LabelLeadingOrganization,
      RemoveIdleLabelOnActivity,
      ResetReviewState,
      ThankContribution,
      CommonRoom,

      # Database processors
      DatabaseReviewExperienceFeedback,

      # Delivery processors
      SecuritySyncApprove,
      VerifyBotApprovals,

      # Engineering Productivity processors
      AddQuarantineLabelsToRelatedIssue,
      PipelineFailureManagement,
      MonitoringEventProcessor,
      GdkIssueLabel,
      UpdateMasterPipelineJobsStatus,
      PipelineIncidentSlaEnforcer,

      # Govern:Threat Insights processors
      AddThreatInsightsGroupLabel,
      RemoveThreatInsightsTeamLabels,
      TagAuthorIfReadyForDevWithoutWeight,

      # Govern:Compliance processors
      AssignComplianceDevForVerification,

      # Support processors
      SupportTeamContributions,

      Workflow::ClosedIssueWorkflowLabelUpdater,
      Workflow::CompleteWhenMrClosesIssue,
      Workflow::DevopsLabelsNudger,
      Workflow::InfradevIssueLabelNudger,
      Workflow::NotifyMergedMasterBrokenMergeRequest,
      # Temporarily disabled pending decisions from https://gitlab.com/gitlab-org/developer-relations/contributor-success/team-task/-/issues/382
      # Workflow::NewIssueWorkflowLabeller,

      # Growth team processors
      AddThreadOnRefinement,
      SetMilestoneOnDev,

      # Docs team processors
      Docs::TechnicalWritingLabelApproved,
      Docs::TechnicalWritingLabelMerged,

      # Privacy team processors
      DeniedDataSubjectRequest,

      # Analytics Instrumentation team processors
      AnalyticsInstrumentationExperienceFeedback
    ].freeze

    Result = Struct.new(:message, :error, :duration)

    def initialize(event, processors: DEFAULT_PROCESSORS)
      @event = event
      @processors = processors
    end

    def process
      results = Hash.new { |h, k| h[k] = Result.new }

      listeners[event.key].each do |processor|
        start_time = Triage.current_monotonic_time
        results[processor.name].message = processor.triage(event)
      rescue StandardError => e
        results[processor.name].error = e
        Raven.capture_exception(e)
      ensure
        results[processor.name].duration = (Triage.current_monotonic_time - start_time).round(5)
      end

      results.select { |processor, result| result.message || result.error }
    end

    private

    attr_reader :event, :processors

    def listeners
      @listeners ||= processors.each_with_object(Hash.new { |h, k| h[k] = [] }) do |processor, result|
        processor.listeners.each do |listener|
          result[listener.event] << processor
        end
      end
    end
  end
end
