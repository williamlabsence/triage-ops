# frozen_string_literal: true

require_relative 'feature_flag_config_change'

module Triage
  class FeatureFlagDefaultEnabledFalseChange < FeatureFlagConfigChange
    def self.line_patterns
      @line_patterns ||= /^\+\s*default_enabled:\s*false/
    end
  end
end
