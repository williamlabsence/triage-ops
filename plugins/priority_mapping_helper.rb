# frozen_string_literal: true

require_relative '../lib/priority_mapping_helper'

Gitlab::Triage::Resource::Context.include PriorityMappingHelper
