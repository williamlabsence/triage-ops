# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/community_contribution_helper'

RSpec.describe CommunityContributionHelper do
  let(:resource_klass) do
    Class.new do
      include CommunityContributionHelper

      def resource; end
    end
  end

  let(:resource) { resource_klass.new }

  describe '#community_or_contractor_contribution_label' do
    subject(:community_or_contractor_contribution_label) { resource.community_or_contractor_contribution_label }

    before do
      stub_request(:get, "https://gitlab.com/api/v4/groups/62281442/members?per_page=100")
        .to_return(status: 200, body: '[{ "id": 123456 }, { "id": 987654 }]', headers: {})

      allow(resource).to receive(:resource).and_return({ author: { id: author_id } })
    end

    describe 'when contractor' do
      let(:author_id) { 123456 }

      it 'returns correct label' do
        expect(subject).to eq('Contractor Contribution')
      end
    end

    describe 'when not contractor' do
      let(:author_id) { 111111 }

      it 'returns correct label' do
        expect(subject).to eq('Community contribution')
      end
    end
  end
end
