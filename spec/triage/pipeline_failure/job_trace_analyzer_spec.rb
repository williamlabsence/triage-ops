# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/triage/pipeline_failure/job_trace_analyzer'

RSpec.describe Triage::PipelineFailure::JobTraceAnalyzer do
  let(:fixture_path) { '/reactive/job_traces' }
  let(:backend_trace) { read_fixture("#{fixture_path}/rspec_failure.txt") }
  let(:frontend_trace) { read_fixture("#{fixture_path}/jest_failure.txt") }
  let(:workhorse_job_trace) { read_fixture("#{fixture_path}/workhorse-test.txt") }

  subject(:job_trace_analyzer) { described_class.new(job_trace: job_trace) }

  describe '#failure_root_cause' do
    context 'with empty trace' do
      let(:job_trace) { '' }

      it 'returns :default' do
        expect(job_trace_analyzer.failure_root_cause).to eq(:default)
      end
    end

    context 'with frontend trace' do
      let(:job_trace) { frontend_trace }

      it 'returns :default' do
        expect(job_trace_analyzer.failure_root_cause).to eq(:default)
      end
    end

    context 'with backend trace' do
      let(:job_trace) { backend_trace }

      it 'returns :default' do
        expect(job_trace_analyzer.failure_root_cause).to eq(:default)
      end
    end

    described_class::TRANSIENT_ROOT_CAUSE_TO_TRACE_MAP.each do |failure_cause, failure_strings|
      failure_strings.each do |failure_string|
        context %(with a "#{failure_string}") do
          let(:job_trace) { failure_string }

          it "returns :#{failure_cause}" do
            expect(job_trace_analyzer.failure_root_cause).to eq(failure_cause)
          end
        end
      end
    end
  end

  describe '#failure_type' do
    context 'with empty trace' do
      let(:job_trace) { '' }

      it 'returns nil' do
        expect(job_trace_analyzer.failure_type).to be nil
      end
    end

    context 'with frontend trace' do
      let(:job_trace) { frontend_trace }

      it 'returns :jest' do
        expect(job_trace_analyzer.failure_type).to eq(:jest)
      end
    end

    context 'with backend trace' do
      let(:job_trace) { backend_trace }

      it 'returns :rspec' do
        expect(job_trace_analyzer.failure_type).to eq(:rspec)
      end
    end

    context 'with workhorse trace' do
      let(:job_trace) { workhorse_job_trace }

      it 'returns :workhorse' do
        expect(job_trace_analyzer.failure_type).to eq(:workhorse)
      end
    end
  end

  describe '#failure_label' do
    context 'with empty trace' do
      let(:job_trace) { '' }

      it 'returns nil' do
        expect(job_trace_analyzer.failure_label).to be nil
      end
    end

    context 'with frontend trace' do
      let(:job_trace) { frontend_trace }

      it 'returns :jest' do
        expect(job_trace_analyzer.failure_label).to eq('~frontend')
      end
    end

    context 'with backend trace' do
      let(:job_trace) { backend_trace }

      it 'returns :rspec' do
        expect(job_trace_analyzer.failure_label).to eq('~backend')
      end
    end

    context 'with workhorse trace' do
      let(:job_trace) { workhorse_job_trace }

      it 'returns :workhorse' do
        expect(job_trace_analyzer.failure_label).to eq('~workhorse')
      end
    end
  end

  describe '#failure_trace' do
    context 'with empty trace' do
      let(:job_trace) { '' }

      it 'returns nil' do
        expect(job_trace_analyzer.failure_trace).to be nil
      end
    end

    context 'with frontend trace' do
      let(:job_trace) { frontend_trace }

      it 'returns "front end spec summary"' do
        expect(job_trace_analyzer.failure_trace).to eq(
          <<~MARKDOWN.chomp
          ```javascript
          FAIL spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js
            ● vue_shared/component/markdown/markdown_editor › disabled › disables content editor when disabled prop is true

              : Timeout - Async callback was not invoked within the 5000 ms timeout specified by jest.setTimeout.Timeout - Async callback was not invoked within the 5000 ms timeout specified by jest.setTimeout.Error:

                148 |     });
                149 |
              > 150 |     it('disables content editor when disabled prop is true', async () => {
                    |     ^
                151 |       buildWrapper({ propsData: { disabled: true } });
                152 |
                153 |       await enableContentEditor();

                at new Spec (node_modules/jest-jasmine2/build/jasmine/Spec.js:124:22)
                at Suite.it (spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js:150:5)
                at Suite.describe (spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js:137:3)
                at Object.describe (spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js:19:1)


          Test Suites: 1 failed, 532 passed, 533 total
          Tests:       1 failed, 2 skipped, 32 todo, 7782 passed, 7817 total
          Snapshots:   56 passed, 56 total
          Time:        1332.608 s
          ```
          MARKDOWN
        )
      end
    end

    context 'with backend trace' do
      let(:job_trace) { backend_trace }

      it 'returns rspec test summary' do
        expect(job_trace_analyzer.failure_trace).to eq(
          <<~MARKDOWN.chomp
          ```ruby

            1) Merge request > User merges when pipeline succeeds when there is active pipeline for merge request with auto_merge_labels_mr_widget on enabling Merge when pipeline succeeds when it was enabled and then canceled behaves like Set to auto-merge activator activates the Merge when pipeline succeeds feature
               Got 0 failures and 2 other errors:
               Shared Example Group: "Set to auto-merge activator" called from ./spec/features/merge_request/user_merges_when_pipeline_succeeds_spec.rb:161

               1.1) Failure/Error: super

                    Capybara::ElementNotFound:
                      Unable to find button "Set to auto-merge" that is not disabled

                      Timeout (45s) reached while running a waiting Capybara finder.
                    # ------------------
                    # --- Caused by: ---
                    # Capybara::ElementNotFound:
                    #   Unable to find button "Set to auto-merge" that is not disabled
                    #   ./spec/support/capybara_slow_finder.rb:18:in `synchronize'

          Finished in 2 minutes 6.7 seconds (files took 1 minute 2.1 seconds to load)
          1 example, 1 failure

          Failed examples:

          rspec './spec/features/merge_request/user_merges_when_pipeline_succeeds_spec.rb[1:2:1:3:1:1]' # Merge request > User merges when pipeline succeeds when there is active pipeline for merge request with auto_merge_labels_mr_widget on enabling Merge when pipeline succeeds when it was enabled and then canceled behaves like Set to auto-merge activator activates the Merge when pipeline succeeds feature
          ```
          MARKDOWN
        )
      end
    end

    context 'with workhorse trace' do
      let(:job_trace) { workhorse_job_trace }

      it 'returns workhorse log' do
        expect(job_trace_analyzer.failure_trace).to eq(
          <<~MARKDOWN.chomp
          ```go

          fatal: No names found, cannot describe anything.
          mkdir -p testdata/scratch
          ### Building gitlab-resize-image
          go build -ldflags "-X main.Version=v16.2.0-pre -X main.BuildTime=20230621.181106" -tags "tracer_static tracer_static_jaeger continuous_profiler_stackdriver fips" -o /builds/gitlab-org/gitlab/workhorse/gitlab-resize-image gitlab.com/gitlab-org/gitlab/workhorse/cmd/gitlab-resize-image
          server response: not found: temporarily unavailable
          ```
          MARKDOWN
        )
      end
    end
  end
end
