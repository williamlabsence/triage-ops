# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/revert_mr_template_nudger'

RSpec.describe Triage::RevertMrTemplateNudger do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        from_gitlab_org_gitlab?: true,
        revert_mr?: revert_mr
      }
    end

    let(:revert_mr) { true }
  end

  include_context 'with merge request notes'

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when the MR is not a revert MR' do
      let(:revert_mr) { false }

      include_examples 'event is not applicable'
    end

    context 'when the MR is a revert MR' do
      let(:revert_mr) { true }

      context 'when the ~pipeline:expedite label is set' do
        let(:label_names) { ['pipeline:expedite'] }

        include_examples 'event is not applicable'
      end

      context 'when the ~pipeline:expedite label is not set' do
        let(:label_names) { [] }

        include_examples 'event is applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment' do
      body = <<~MARKDOWN.chomp
        :wave: @joe,

        To speed up your revert merge request, please consider using the [revert merge request template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/merge_request_templates/Revert%20To%20Resolve%20Incident.md). Adding the appropriate labels for resolving ~"master:broken" **before** the merge request is created will skip several CI/CD jobs.

        For this merge request, if this is for resolving ~"master:broken" you can add the appropriate labels present in the merge request template, and trigger a new pipeline. It will be faster :rocket:.

        [See the docs](https://docs.gitlab.com/ee/development/pipelines#revert-mrs).
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
